import logging

mailparser_logger = logging.getLogger('mailparser')
mailparser_logger.setLevel(logging.FATAL)