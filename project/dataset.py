from pathlib import Path
from pandas import read_csv, DataFrame
from project.fs import read_dir
from project.parser import load_email

def load_emails_dataset_from_directory(dataset_file: str ) -> DataFrame:
    
    return read_csv(dataset_file)