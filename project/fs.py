from typing import List
from pathlib import Path

GLOB_PATTERN = "**/*"

def read_dir(directory: Path) -> List[str]: 
    return [ str(x.absolute()) for x in directory.glob(GLOB_PATTERN) if x.is_file()]