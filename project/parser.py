from typing import Dict, Any
from re import search
from mailparser import parse_from_file
from mailparser.mailparser import MailParser

def load_email(file_name: str) -> Dict[str, Any]: 
    return extrat_features(parse_from_file(file_name))
    

def extrat_features(email: MailParser) -> Dict[str, Any]: 
    from_alias, from_email = email.from_.pop()
    
    return {
        'from': from_email, 
        'from_alias': from_alias,
        'from_domain': extract_domain(from_email),
        'date': email.date,
        'subject': email.subject,
        'body': email.body
    }


DOMAIN_EXTRACT_REGEX = r'\w+@(?P<email>\w+\.\w+(\.\w+)*)\b'

def extract_domain(email: str) -> str: 
    result = search(DOMAIN_EXTRACT_REGEX, email)
    
    if result and result.groupdict():
        return result.groupdict().get('email', '')
    
    return ''